package asw01cs;


import java.util.Arrays;

import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
//This code uses the Fluent API


public class SimpleFluentClient {

	private static String URI = "http://localhost:8080/waslab01_ss/";

	public final static void main(String[] args) throws Exception {
    	
    	/* Insert code for Task #4 here */
		
		Content result = Request.Post(URI).bodyForm(Form.form().add("author",  "Marti").add("tweet_text",  "This tweet is not gona be in the web").build())
		.addHeader("Accept", "text/plain").execute().returnContent();
		
		System.out.println(result);
		
    	System.out.println(Request.Get(URI).addHeader("Accept", "text/plain").execute().returnContent());
    	
    	Request.Post(URI).bodyForm(Form.form().add("action",  "delete").add("tweet_id",  result.toString()).build())
    			.addHeader("Accept", "text/plain").execute().returnContent();
    	/* Insert code for Task #5 here */
    }
}

